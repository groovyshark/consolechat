#include <iostream>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "SimpleIni.h"
#include "config.h"

sf::TcpSocket socket;
sf::Mutex mutex;
bool disconnect = false;

std::string messageSend;

void handle();
void getInput();


int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Error: first argument must be .ini file!\n";
        return 1;
    }
    params.init(argv[1]);


    sf::Thread* thread = nullptr;

    /**
     * Connect to remote application, become a Client
     * If we could not connect, we starting to wait connection, become a Server
     * */
    if (socket.connect(params.remoteIP, params.remotePort) != sf::Socket::Done)
    {
        sf::TcpListener listener;
        std::cout << "Wait connection on "<< params.generalPort << " port" << std::endl;

        /// Bind the listener to a port
        if (listener.listen(params.generalPort) != sf::Socket::Done)
        {
            std::cerr << "Can't bind the client...\n";
            return 1;
        }

        if (listener.accept(socket) != sf::Socket::Done)
        {
            std::cerr << "Can't accept the client...\n";
            return 1;
        }
        std::cout << "New client connected: " << socket.getRemoteAddress() << std::endl;

        mutex.lock();
        messageSend = params.initMessage;
        mutex.unlock();
    }

    /// Launch process messaging in other thread
    thread = new sf::Thread(&handle);
    thread->launch();

    /// In main thread, read from keyboard
    while(!disconnect)
    {
        getInput();
    }

    /// Stop and delete thread
    if (thread)
    {
        thread->wait();
        delete thread;
    }

    return 0;
}

void handle()
{
    static std::string messageOld;
    while(!disconnect)
    {
        sf::Packet packetSend;

        /**
         * Write our message in packet
         * Since variable 'messageSend' is shared,
         * we use the mutex
         * */
        mutex.lock();
        packetSend << messageSend;
        mutex.unlock();

        /// Try to send packet
        if(socket.send(packetSend) == sf::Socket::Status::Disconnected)
        {
            disconnect = true;
            break;
        }

        /// Try to receive packet
        sf::Packet packetReceive;
        if (socket.receive(packetReceive) == sf::Socket::Status::Disconnected)
        {
            disconnect = true;
            break;
        }

        std::string messageReceive;

        /// Write packet data in std::string variable
        if(packetReceive >> messageReceive)
        {
            if(messageOld != messageReceive)
                if(!messageReceive.empty())
                {
                    std::cout<<socket.getRemoteAddress().toString()<< ": " << messageReceive << std::endl;
                    messageOld = messageReceive;
                }
        }
    }

    std::cout << "Disconnect...\n";
    socket.disconnect();
}

void getInput()
{
    std::string message;
    std::getline(std::cin, message);

    if(message == "#")
        disconnect = true;

    mutex.lock();
    messageSend = message;
    mutex.unlock();
}