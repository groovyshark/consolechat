//
// Created by rzhevskiy on 05.08.17.
//

#pragma once

#include <string>
#include "SimpleIni.h"


struct ConnectionParams
{
    int         generalPort;
    std::string remoteIP;
    int         remotePort;
    std::string initMessage;

    void init(const std::string& filename)
    {
        CSimpleIniA ini;

        ini.SetUnicode();
        ini.LoadFile(filename.c_str());

        generalPort = atoi(ini.GetValue("General", "Port"));
        remoteIP    = ini.GetValue("Remote", "IP");
        remotePort  = atoi(ini.GetValue("Remote", "Port"));
        initMessage = ini.GetValue("Initial", "Message");

        ini.Reset();
    }

} params;

